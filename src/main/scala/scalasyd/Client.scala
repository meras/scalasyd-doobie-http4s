package scalasyd

import dispatch._
import dispatch.Defaults._

object Client {

  val findBandsWithArtists = url("http://localhost:8080/bands/artists")

  def main(args: Array[String]): Unit = {
    Http(findBandsWithArtists OK as.String) foreach println
  }

}